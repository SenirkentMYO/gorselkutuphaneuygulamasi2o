﻿namespace KutuphaneProjesi
{
    partial class KullaniciKayit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblid = new System.Windows.Forms.Label();
            this.lbladi = new System.Windows.Forms.Label();
            this.lblsayadi = new System.Windows.Forms.Label();
            this.lblemail = new System.Windows.Forms.Label();
            this.txtadi = new System.Windows.Forms.TextBox();
            this.txtsoyadi = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.txtid = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Location = new System.Drawing.Point(31, 42);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(21, 13);
            this.lblid.TabIndex = 0;
            this.lblid.Text = "ID:";
            // 
            // lbladi
            // 
            this.lbladi.AutoSize = true;
            this.lbladi.Location = new System.Drawing.Point(31, 93);
            this.lbladi.Name = "lbladi";
            this.lbladi.Size = new System.Drawing.Size(25, 13);
            this.lbladi.TabIndex = 1;
            this.lbladi.Text = "Adı:";
            // 
            // lblsayadi
            // 
            this.lblsayadi.AutoSize = true;
            this.lblsayadi.Location = new System.Drawing.Point(31, 141);
            this.lblsayadi.Name = "lblsayadi";
            this.lblsayadi.Size = new System.Drawing.Size(42, 13);
            this.lblsayadi.TabIndex = 2;
            this.lblsayadi.Text = "Soyadı:";
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.Location = new System.Drawing.Point(31, 190);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(41, 13);
            this.lblemail.TabIndex = 3;
            this.lblemail.Text = "E- mail:";
            // 
            // txtadi
            // 
            this.txtadi.Location = new System.Drawing.Point(112, 93);
            this.txtadi.Name = "txtadi";
            this.txtadi.Size = new System.Drawing.Size(100, 20);
            this.txtadi.TabIndex = 5;
            // 
            // txtsoyadi
            // 
            this.txtsoyadi.Location = new System.Drawing.Point(112, 141);
            this.txtsoyadi.Name = "txtsoyadi";
            this.txtsoyadi.Size = new System.Drawing.Size(100, 20);
            this.txtsoyadi.TabIndex = 6;
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(112, 190);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(100, 20);
            this.txtemail.TabIndex = 7;
            // 
            // btnkaydet
            // 
            this.btnkaydet.Location = new System.Drawing.Point(204, 276);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(75, 23);
            this.btnkaydet.TabIndex = 8;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(112, 42);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(100, 20);
            this.txtid.TabIndex = 9;
            // 
            // KullaniciKayit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 353);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.btnkaydet);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtsoyadi);
            this.Controls.Add(this.txtadi);
            this.Controls.Add(this.lblemail);
            this.Controls.Add(this.lblsayadi);
            this.Controls.Add(this.lbladi);
            this.Controls.Add(this.lblid);
            this.Name = "KullaniciKayit";
            this.Text = "KullaniciKayit";
            this.Load += new System.EventHandler(this.KullaniciKayit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblid;
        private System.Windows.Forms.Label lbladi;
        private System.Windows.Forms.Label lblsayadi;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.TextBox txtadi;
        private System.Windows.Forms.TextBox txtsoyadi;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Button btnkaydet;
        private System.Windows.Forms.TextBox txtid;
    }
}