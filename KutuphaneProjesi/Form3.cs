﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KutuphaneProjesi
{
    public partial class Form3 : Form
    {
        Kullanici giren = null;

        public Form3(Kullanici k)
        {
            InitializeComponent();

            giren = k;
        }

        private void btnKayitGetir_Click(object sender, EventArgs e)
        {
            List<Kitap>Ktp=new List<Kitap>();
            KutuphaneProjesiEntities dbase=new KutuphaneProjesiEntities();

            Ktp = dbase.Kitap.ToList();
            dgvKayitGetir.DataSource = Ktp;


        }

        private void Ara_Click(object sender, EventArgs e)
        {
            List<Kitap> Ktp = new List<Kitap>();
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            Ktp = dbase.Kitap.Where(arama => arama.Adi.Contains(txtara.Text)).ToList();
            dgvKayitGetir.DataSource = Ktp;
        }

        private void txtara_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.OrderByDescending(a=>a.OkunmaSayisi).Take(10).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btntoplamkitap_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            var toplam = liste.Sum(a => a.StokSayisi);

            MessageBox.Show(toplam.ToString());
        }

        private void btnaz_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            List<Kitap> liste = dbase.Kitap.OrderBy(a => a.Adi).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btnza_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            List<Kitap> liste = dbase.Kitap.OrderByDescending(a => a.Adi).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btnyenikitap_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            List<Kitap> linqlist = (from k in liste where k.Basimyili > 2013 select k).ToList();
            dgvKayitGetir.DataSource = linqlist;

        }

        private void btnYazarlar_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            var linqlist = (from s in liste where s.ID >=0 select new { Kitap = s.Adi , Stok = s.StokSayisi }).ToList();
            dgvKayitGetir.DataSource = linqlist;
            

        }

        private void dgvKayitGetir_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities db = new KutuphaneProjesiEntities();
            dgvKayitGetir.DataSource = db.Kitap.ToList();

            this.Text = giren.Adi + " " + giren.Soyadi;
        }
    }
}
