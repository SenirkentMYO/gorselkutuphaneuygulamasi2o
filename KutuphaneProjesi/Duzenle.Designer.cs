﻿namespace KutuphaneProjesi
{
    partial class Duzenle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblkitapid = new System.Windows.Forms.Label();
            this.lblkitapadi = new System.Windows.Forms.Label();
            this.txtkitapid = new System.Windows.Forms.TextBox();
            this.txtkitapadi = new System.Windows.Forms.TextBox();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblkitapid
            // 
            this.lblkitapid.AutoSize = true;
            this.lblkitapid.Location = new System.Drawing.Point(25, 54);
            this.lblkitapid.Name = "lblkitapid";
            this.lblkitapid.Size = new System.Drawing.Size(48, 13);
            this.lblkitapid.TabIndex = 0;
            this.lblkitapid.Text = "Kitap ID:";
            // 
            // lblkitapadi
            // 
            this.lblkitapadi.AutoSize = true;
            this.lblkitapadi.Location = new System.Drawing.Point(28, 88);
            this.lblkitapadi.Name = "lblkitapadi";
            this.lblkitapadi.Size = new System.Drawing.Size(52, 13);
            this.lblkitapadi.TabIndex = 1;
            this.lblkitapadi.Text = "Kitap Adı:";
            // 
            // txtkitapid
            // 
            this.txtkitapid.Location = new System.Drawing.Point(98, 51);
            this.txtkitapid.Name = "txtkitapid";
            this.txtkitapid.Size = new System.Drawing.Size(100, 20);
            this.txtkitapid.TabIndex = 2;
            // 
            // txtkitapadi
            // 
            this.txtkitapadi.Location = new System.Drawing.Point(98, 85);
            this.txtkitapadi.Name = "txtkitapadi";
            this.txtkitapadi.Size = new System.Drawing.Size(100, 20);
            this.txtkitapadi.TabIndex = 3;
            // 
            // btnkaydet
            // 
            this.btnkaydet.Location = new System.Drawing.Point(122, 167);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(75, 23);
            this.btnkaydet.TabIndex = 4;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            // 
            // Duzenle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 261);
            this.Controls.Add(this.btnkaydet);
            this.Controls.Add(this.txtkitapadi);
            this.Controls.Add(this.txtkitapid);
            this.Controls.Add(this.lblkitapadi);
            this.Controls.Add(this.lblkitapid);
            this.Name = "Duzenle";
            this.Text = "Duzenle";
            this.Load += new System.EventHandler(this.Duzenle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblkitapid;
        private System.Windows.Forms.Label lblkitapadi;
        private System.Windows.Forms.TextBox txtkitapid;
        private System.Windows.Forms.TextBox txtkitapadi;
        private System.Windows.Forms.Button btnkaydet;
    }
}