﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KutuphaneProjesi
{
    public partial class KullaniciGiris : Form
    {
        public KullaniciGiris()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void giris_Click(object sender, EventArgs e)
        {
            List<Kullanici> Liste = new List<Kullanici>();
            KutuphaneProjesiEntities kitap = new KutuphaneProjesiEntities();

            Liste = kitap.Kullanici.Where(kullanicipaneli => kullanicipaneli.Email == txtemail.Text && kullanicipaneli.Sifre == txtSifre.Text).ToList();
            if (Liste.Count == 1)
            {
                Form3 a = new Form3(Liste[0]);
                a.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Hatalı Giriş");


            }
        }


        private void btnyoneticigiris_Click(object sender, EventArgs e)
        {
            List<Yonetici> YoneticiListe = new List<Yonetici>();
            KutuphaneProjesiEntities kitap = new KutuphaneProjesiEntities();

            YoneticiListe = kitap.Yonetici.Where(yoneticipaneli => yoneticipaneli.Email == txtyoneticiemail.Text && yoneticipaneli.Sifre == txtyoneticisifre.Text).ToList();

            if (YoneticiListe.Count == 1)
            {
                Yoneticipaneli a = new Yoneticipaneli(YoneticiListe[0]);
                a.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Hatalı Giriş");


            }
        }

        private void KullaniciGiris_Load(object sender, EventArgs e)
        {

        }
    }
}
