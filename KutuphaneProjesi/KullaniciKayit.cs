﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KutuphaneProjesi
{
    public partial class KullaniciKayit : Form
    {
        Kullanici klnc;
        public KullaniciKayit(Kullanici k)
        {
            klnc = k;
            InitializeComponent();

        }


        private void KullaniciKayit_Load(object sender, EventArgs e)
        {
            txtid.Text = klnc.ID.ToString();
            txtadi.Text = klnc.Adi;
            txtsoyadi.Text = klnc.Soyadi;
            txtemail.Text = klnc.Email;

        }
    }
}
