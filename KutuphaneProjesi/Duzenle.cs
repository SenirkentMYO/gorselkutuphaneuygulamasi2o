﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KutuphaneProjesi
{
    public partial class Duzenle : Form
    {
        Kitap kt;

        public Duzenle(Kitap k)
        {
            kt = k;
            InitializeComponent();
        }

        private void Duzenle_Load(object sender, EventArgs e)
        {
            txtkitapid.Text = kt.ID.ToString();
            txtkitapadi.Text = kt.Adi;
        }
    }
}
