﻿namespace KutuphaneProjesi
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvKayitGetir = new System.Windows.Forms.DataGridView();
            this.btnKayitGetir = new System.Windows.Forms.Button();
            this.Ara = new System.Windows.Forms.Button();
            this.txtara = new System.Windows.Forms.TextBox();
            this.lblKitapAra = new System.Windows.Forms.Label();
            this.btnEnCokOkunanlar = new System.Windows.Forms.Button();
            this.btntoplamkitap = new System.Windows.Forms.Button();
            this.lblsirala = new System.Windows.Forms.Label();
            this.btnaz = new System.Windows.Forms.Button();
            this.btnza = new System.Windows.Forms.Button();
            this.btnyenikitap = new System.Windows.Forms.Button();
            this.btnStok = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKayitGetir)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvKayitGetir
            // 
            this.dgvKayitGetir.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKayitGetir.Location = new System.Drawing.Point(12, 37);
            this.dgvKayitGetir.Name = "dgvKayitGetir";
            this.dgvKayitGetir.Size = new System.Drawing.Size(240, 150);
            this.dgvKayitGetir.TabIndex = 0;
            this.dgvKayitGetir.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKayitGetir_CellContentClick);
            // 
            // btnKayitGetir
            // 
            this.btnKayitGetir.Location = new System.Drawing.Point(12, 312);
            this.btnKayitGetir.Name = "btnKayitGetir";
            this.btnKayitGetir.Size = new System.Drawing.Size(102, 23);
            this.btnKayitGetir.TabIndex = 1;
            this.btnKayitGetir.Text = "Kayıt Getir";
            this.btnKayitGetir.UseVisualStyleBackColor = true;
            this.btnKayitGetir.Click += new System.EventHandler(this.btnKayitGetir_Click);
            // 
            // Ara
            // 
            this.Ara.Location = new System.Drawing.Point(177, 8);
            this.Ara.Name = "Ara";
            this.Ara.Size = new System.Drawing.Size(75, 23);
            this.Ara.TabIndex = 2;
            this.Ara.Text = "Ara";
            this.Ara.UseVisualStyleBackColor = true;
            this.Ara.Click += new System.EventHandler(this.Ara_Click);
            // 
            // txtara
            // 
            this.txtara.Location = new System.Drawing.Point(68, 9);
            this.txtara.Name = "txtara";
            this.txtara.Size = new System.Drawing.Size(100, 20);
            this.txtara.TabIndex = 3;
            this.txtara.TextChanged += new System.EventHandler(this.txtara_TextChanged);
            // 
            // lblKitapAra
            // 
            this.lblKitapAra.AutoSize = true;
            this.lblKitapAra.Location = new System.Drawing.Point(12, 9);
            this.lblKitapAra.Name = "lblKitapAra";
            this.lblKitapAra.Size = new System.Drawing.Size(50, 13);
            this.lblKitapAra.TabIndex = 4;
            this.lblKitapAra.Text = "Kitap Ara";
            // 
            // btnEnCokOkunanlar
            // 
            this.btnEnCokOkunanlar.Location = new System.Drawing.Point(12, 233);
            this.btnEnCokOkunanlar.Name = "btnEnCokOkunanlar";
            this.btnEnCokOkunanlar.Size = new System.Drawing.Size(102, 23);
            this.btnEnCokOkunanlar.TabIndex = 5;
            this.btnEnCokOkunanlar.Text = "En Çok Okunanlar";
            this.btnEnCokOkunanlar.UseVisualStyleBackColor = true;
            this.btnEnCokOkunanlar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btntoplamkitap
            // 
            this.btntoplamkitap.Location = new System.Drawing.Point(12, 275);
            this.btntoplamkitap.Name = "btntoplamkitap";
            this.btntoplamkitap.Size = new System.Drawing.Size(102, 23);
            this.btntoplamkitap.TabIndex = 6;
            this.btntoplamkitap.Text = "Toplam Kitap";
            this.btntoplamkitap.UseVisualStyleBackColor = true;
            this.btntoplamkitap.Click += new System.EventHandler(this.btntoplamkitap_Click);
            // 
            // lblsirala
            // 
            this.lblsirala.AutoSize = true;
            this.lblsirala.Location = new System.Drawing.Point(174, 203);
            this.lblsirala.Name = "lblsirala";
            this.lblsirala.Size = new System.Drawing.Size(36, 13);
            this.lblsirala.TabIndex = 7;
            this.lblsirala.Text = "Sırala:";
            // 
            // btnaz
            // 
            this.btnaz.Location = new System.Drawing.Point(177, 233);
            this.btnaz.Name = "btnaz";
            this.btnaz.Size = new System.Drawing.Size(75, 23);
            this.btnaz.TabIndex = 8;
            this.btnaz.Text = "A - Z";
            this.btnaz.UseVisualStyleBackColor = true;
            this.btnaz.Click += new System.EventHandler(this.btnaz_Click);
            // 
            // btnza
            // 
            this.btnza.Location = new System.Drawing.Point(177, 275);
            this.btnza.Name = "btnza";
            this.btnza.Size = new System.Drawing.Size(75, 23);
            this.btnza.TabIndex = 9;
            this.btnza.Text = "Z - A";
            this.btnza.UseVisualStyleBackColor = true;
            this.btnza.Click += new System.EventHandler(this.btnza_Click);
            // 
            // btnyenikitap
            // 
            this.btnyenikitap.Location = new System.Drawing.Point(12, 198);
            this.btnyenikitap.Name = "btnyenikitap";
            this.btnyenikitap.Size = new System.Drawing.Size(102, 23);
            this.btnyenikitap.TabIndex = 10;
            this.btnyenikitap.Text = "Yeni Kitaplar";
            this.btnyenikitap.UseVisualStyleBackColor = true;
            this.btnyenikitap.Click += new System.EventHandler(this.btnyenikitap_Click);
            // 
            // btnStok
            // 
            this.btnStok.Location = new System.Drawing.Point(177, 312);
            this.btnStok.Name = "btnStok";
            this.btnStok.Size = new System.Drawing.Size(75, 23);
            this.btnStok.TabIndex = 11;
            this.btnStok.Text = "Stok";
            this.btnStok.UseVisualStyleBackColor = true;
            this.btnStok.Click += new System.EventHandler(this.btnYazarlar_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 396);
            this.Controls.Add(this.btnStok);
            this.Controls.Add(this.btnyenikitap);
            this.Controls.Add(this.btnza);
            this.Controls.Add(this.btnaz);
            this.Controls.Add(this.lblsirala);
            this.Controls.Add(this.btntoplamkitap);
            this.Controls.Add(this.btnEnCokOkunanlar);
            this.Controls.Add(this.lblKitapAra);
            this.Controls.Add(this.txtara);
            this.Controls.Add(this.Ara);
            this.Controls.Add(this.btnKayitGetir);
            this.Controls.Add(this.dgvKayitGetir);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKayitGetir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvKayitGetir;
        private System.Windows.Forms.Button btnKayitGetir;
        private System.Windows.Forms.Button Ara;
        private System.Windows.Forms.TextBox txtara;
        private System.Windows.Forms.Label lblKitapAra;
        private System.Windows.Forms.Button btnEnCokOkunanlar;
        private System.Windows.Forms.Button btntoplamkitap;
        private System.Windows.Forms.Label lblsirala;
        private System.Windows.Forms.Button btnaz;
        private System.Windows.Forms.Button btnza;
        private System.Windows.Forms.Button btnyenikitap;
        private System.Windows.Forms.Button btnStok;
    }
}