﻿namespace KutuphaneProjesi
{
    partial class Yoneticipaneli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnyenikitap = new System.Windows.Forms.Button();
            this.dgvKayitGetir = new System.Windows.Forms.DataGridView();
            this.btnKayitGetir = new System.Windows.Forms.Button();
            this.Ara = new System.Windows.Forms.Button();
            this.txtara = new System.Windows.Forms.TextBox();
            this.btnEnCokOkunanlar = new System.Windows.Forms.Button();
            this.btntoplamkitap = new System.Windows.Forms.Button();
            this.lblsirala = new System.Windows.Forms.Label();
            this.btnaz = new System.Windows.Forms.Button();
            this.btnza = new System.Windows.Forms.Button();
            this.btnStok = new System.Windows.Forms.Button();
            this.btnduzenle = new System.Windows.Forms.Button();
            this.btnkullanicikaydi = new System.Windows.Forms.Button();
            this.btnkullaniciduzenle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKayitGetir)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kitap Ara";
            // 
            // btnyenikitap
            // 
            this.btnyenikitap.Location = new System.Drawing.Point(26, 194);
            this.btnyenikitap.Name = "btnyenikitap";
            this.btnyenikitap.Size = new System.Drawing.Size(102, 23);
            this.btnyenikitap.TabIndex = 11;
            this.btnyenikitap.Text = "Yeni Kitaplar";
            this.btnyenikitap.UseVisualStyleBackColor = true;
            this.btnyenikitap.Click += new System.EventHandler(this.btnyenikitap_Click);
            // 
            // dgvKayitGetir
            // 
            this.dgvKayitGetir.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKayitGetir.Location = new System.Drawing.Point(26, 38);
            this.dgvKayitGetir.Name = "dgvKayitGetir";
            this.dgvKayitGetir.Size = new System.Drawing.Size(240, 150);
            this.dgvKayitGetir.TabIndex = 13;
            this.dgvKayitGetir.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKayitGetir_CellContentClick);
            // 
            // btnKayitGetir
            // 
            this.btnKayitGetir.Location = new System.Drawing.Point(164, 194);
            this.btnKayitGetir.Name = "btnKayitGetir";
            this.btnKayitGetir.Size = new System.Drawing.Size(102, 23);
            this.btnKayitGetir.TabIndex = 14;
            this.btnKayitGetir.Text = "Kayıt Getir";
            this.btnKayitGetir.UseVisualStyleBackColor = true;
            this.btnKayitGetir.Click += new System.EventHandler(this.btnKayitGetir_Click);
            // 
            // Ara
            // 
            this.Ara.Location = new System.Drawing.Point(191, 12);
            this.Ara.Name = "Ara";
            this.Ara.Size = new System.Drawing.Size(75, 23);
            this.Ara.TabIndex = 15;
            this.Ara.Text = "Ara";
            this.Ara.UseVisualStyleBackColor = true;
            this.Ara.Click += new System.EventHandler(this.Ara_Click);
            // 
            // txtara
            // 
            this.txtara.Location = new System.Drawing.Point(82, 12);
            this.txtara.Name = "txtara";
            this.txtara.Size = new System.Drawing.Size(100, 20);
            this.txtara.TabIndex = 16;
            // 
            // btnEnCokOkunanlar
            // 
            this.btnEnCokOkunanlar.Location = new System.Drawing.Point(26, 223);
            this.btnEnCokOkunanlar.Name = "btnEnCokOkunanlar";
            this.btnEnCokOkunanlar.Size = new System.Drawing.Size(102, 23);
            this.btnEnCokOkunanlar.TabIndex = 17;
            this.btnEnCokOkunanlar.Text = "En Çok Okunanlar";
            this.btnEnCokOkunanlar.UseVisualStyleBackColor = true;
            this.btnEnCokOkunanlar.Click += new System.EventHandler(this.btnEnCokOkunanlar_Click_1);
            // 
            // btntoplamkitap
            // 
            this.btntoplamkitap.Location = new System.Drawing.Point(26, 252);
            this.btntoplamkitap.Name = "btntoplamkitap";
            this.btntoplamkitap.Size = new System.Drawing.Size(102, 23);
            this.btntoplamkitap.TabIndex = 18;
            this.btntoplamkitap.Text = "Toplam Kitap";
            this.btntoplamkitap.UseVisualStyleBackColor = true;
            this.btntoplamkitap.Click += new System.EventHandler(this.btntoplamkitap_Click);
            // 
            // lblsirala
            // 
            this.lblsirala.AutoSize = true;
            this.lblsirala.Location = new System.Drawing.Point(161, 286);
            this.lblsirala.Name = "lblsirala";
            this.lblsirala.Size = new System.Drawing.Size(36, 13);
            this.lblsirala.TabIndex = 19;
            this.lblsirala.Text = "Sırala:";
            // 
            // btnaz
            // 
            this.btnaz.Location = new System.Drawing.Point(164, 310);
            this.btnaz.Name = "btnaz";
            this.btnaz.Size = new System.Drawing.Size(102, 23);
            this.btnaz.TabIndex = 20;
            this.btnaz.Text = "A - Z";
            this.btnaz.UseVisualStyleBackColor = true;
            this.btnaz.Click += new System.EventHandler(this.btnaz_Click);
            // 
            // btnza
            // 
            this.btnza.Location = new System.Drawing.Point(164, 344);
            this.btnza.Name = "btnza";
            this.btnza.Size = new System.Drawing.Size(102, 23);
            this.btnza.TabIndex = 21;
            this.btnza.Text = "Z - A";
            this.btnza.UseVisualStyleBackColor = true;
            this.btnza.Click += new System.EventHandler(this.btnza_Click);
            // 
            // btnStok
            // 
            this.btnStok.Location = new System.Drawing.Point(26, 281);
            this.btnStok.Name = "btnStok";
            this.btnStok.Size = new System.Drawing.Size(102, 23);
            this.btnStok.TabIndex = 22;
            this.btnStok.Text = "Stok";
            this.btnStok.UseVisualStyleBackColor = true;
            this.btnStok.Click += new System.EventHandler(this.btnStok_Click);
            // 
            // btnduzenle
            // 
            this.btnduzenle.Location = new System.Drawing.Point(26, 310);
            this.btnduzenle.Name = "btnduzenle";
            this.btnduzenle.Size = new System.Drawing.Size(102, 23);
            this.btnduzenle.TabIndex = 23;
            this.btnduzenle.Text = "Düzenle";
            this.btnduzenle.UseVisualStyleBackColor = true;
            this.btnduzenle.Click += new System.EventHandler(this.btnduzenle_Click);
            // 
            // btnkullanicikaydi
            // 
            this.btnkullanicikaydi.Location = new System.Drawing.Point(164, 223);
            this.btnkullanicikaydi.Name = "btnkullanicikaydi";
            this.btnkullanicikaydi.Size = new System.Drawing.Size(102, 39);
            this.btnkullanicikaydi.TabIndex = 24;
            this.btnkullanicikaydi.Text = "Kullanıcı Kaydı Getirme";
            this.btnkullanicikaydi.UseVisualStyleBackColor = true;
            this.btnkullanicikaydi.Click += new System.EventHandler(this.btnkullanicikaydi_Click);
            // 
            // btnkullaniciduzenle
            // 
            this.btnkullaniciduzenle.Location = new System.Drawing.Point(26, 344);
            this.btnkullaniciduzenle.Name = "btnkullaniciduzenle";
            this.btnkullaniciduzenle.Size = new System.Drawing.Size(102, 23);
            this.btnkullaniciduzenle.TabIndex = 25;
            this.btnkullaniciduzenle.Text = "Kullanıcı Düzenle";
            this.btnkullaniciduzenle.UseVisualStyleBackColor = true;
            this.btnkullaniciduzenle.Click += new System.EventHandler(this.btnkullaniciduzenle_Click);
            // 
            // Yoneticipaneli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 379);
            this.Controls.Add(this.btnkullaniciduzenle);
            this.Controls.Add(this.btnkullanicikaydi);
            this.Controls.Add(this.btnduzenle);
            this.Controls.Add(this.btnStok);
            this.Controls.Add(this.btnza);
            this.Controls.Add(this.btnaz);
            this.Controls.Add(this.lblsirala);
            this.Controls.Add(this.btntoplamkitap);
            this.Controls.Add(this.btnEnCokOkunanlar);
            this.Controls.Add(this.txtara);
            this.Controls.Add(this.Ara);
            this.Controls.Add(this.btnKayitGetir);
            this.Controls.Add(this.dgvKayitGetir);
            this.Controls.Add(this.btnyenikitap);
            this.Controls.Add(this.label1);
            this.Name = "Yoneticipaneli";
            this.Text = "Yoneticipaneli";
            this.Load += new System.EventHandler(this.Yoneticipaneli_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKayitGetir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnyenikitap;
        private System.Windows.Forms.DataGridView dgvKayitGetir;
        private System.Windows.Forms.Button btnKayitGetir;
        private System.Windows.Forms.Button Ara;
        private System.Windows.Forms.TextBox txtara;
        private System.Windows.Forms.Button btnEnCokOkunanlar;
        private System.Windows.Forms.Button btntoplamkitap;
        private System.Windows.Forms.Label lblsirala;
        private System.Windows.Forms.Button btnaz;
        private System.Windows.Forms.Button btnza;
        private System.Windows.Forms.Button btnStok;
        private System.Windows.Forms.Button btnduzenle;
        private System.Windows.Forms.Button btnkullanicikaydi;
        private System.Windows.Forms.Button btnkullaniciduzenle;
    }
}