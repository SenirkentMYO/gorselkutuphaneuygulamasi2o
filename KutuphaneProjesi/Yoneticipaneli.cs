﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KutuphaneProjesi
{
    public partial class Yoneticipaneli : Form
    {
        Yonetici giren = null;

        public Yoneticipaneli(Yonetici y)
        {
            InitializeComponent();

            giren = y;
        }

        private void dgvKayitGetir_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            List<Kitap> Ktp = new List<Kitap>();
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            Ktp = dbase.Kitap.ToList();
            dgvKayitGetir.DataSource = Ktp;
        }

        private void Ara_Click(object sender, EventArgs e)
        {
            List<Kitap> Ktp = new List<Kitap>();
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            Ktp = dbase.Kitap.Where(arama => arama.Adi.Contains(txtara.Text)).ToList();
            dgvKayitGetir.DataSource = Ktp;
        }

        private void btnyenikitap_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            List<Kitap> linqlist = (from k in liste where k.Basimyili > 2013 select k).ToList();
            dgvKayitGetir.DataSource = linqlist;
        }

        private void btnEnCokOkunanlar_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.OrderByDescending(a => a.OkunmaSayisi).Take(10).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btnKayitGetir_Click(object sender, EventArgs e)
        {
            List<Kitap> Ktp = new List<Kitap>();
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            Ktp = dbase.Kitap.ToList();
            dgvKayitGetir.DataSource = Ktp;
        }

        private void btnEnCokOkunanlar_Click_1(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.OrderByDescending(a => a.OkunmaSayisi).Take(10).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btntoplamkitap_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            var toplam = liste.Sum(a => a.StokSayisi);

            MessageBox.Show(toplam.ToString());
        }

        private void btnaz_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            List<Kitap> liste = dbase.Kitap.OrderBy(a => a.Adi).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btnza_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            List<Kitap> liste = dbase.Kitap.OrderByDescending(a => a.Adi).ToList();
            dgvKayitGetir.DataSource = liste;
        }

        private void btnStok_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            List<Kitap> liste = dbase.Kitap.ToList();

            var linqlist = (from s in liste where s.ID >= 0 select new { Kitap = s.Adi, Stok = s.StokSayisi }).ToList();
            dgvKayitGetir.DataSource = linqlist;
        }

        private void btnduzenle_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            int secildenid = Convert.ToInt32(dgvKayitGetir.SelectedRows[0].Cells[0].Value.ToString());
            Kitap ktp = dbase.Kitap.Where(a => a.ID == secildenid).First();
            Duzenle duzen = new Duzenle(ktp);
            duzen.Show();
        }

        private void btnkullanicikaydi_Click(object sender, EventArgs e)
        {
            List<Kullanici> kllnc = new List<Kullanici>();
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();

            kllnc = dbase.Kullanici.ToList();
            dgvKayitGetir.DataSource = kllnc;
            
        }

        private void btnkullaniciduzenle_Click(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities dbase = new KutuphaneProjesiEntities();
            int secildenid = Convert.ToInt32(dgvKayitGetir.SelectedRows[0].Cells[0].Value.ToString());
            Kullanici klnc = dbase.Kullanici.Where(a => a.ID == secildenid).First();
            KullaniciKayit kullaniciduzen = new KullaniciKayit(klnc);
            kullaniciduzen.Show();
        }

        private void Yoneticipaneli_Load(object sender, EventArgs e)
        {
            KutuphaneProjesiEntities db = new KutuphaneProjesiEntities();
            dgvKayitGetir.DataSource = db.Kitap.ToList();

            this.Text = giren.Adi + " " + giren.Soyadi;
        }
    }
}
