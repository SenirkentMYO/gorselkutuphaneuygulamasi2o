﻿namespace KutuphaneProjesi
{
    partial class KullaniciGiris
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblemail = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.lblsifre = new System.Windows.Forms.Label();
            this.txtSifre = new System.Windows.Forms.TextBox();
            this.giris = new System.Windows.Forms.Button();
            this.lblkullanici = new System.Windows.Forms.Label();
            this.lblyonetici = new System.Windows.Forms.Label();
            this.lblyoneticiemail = new System.Windows.Forms.Label();
            this.lblyoneticisifre = new System.Windows.Forms.Label();
            this.txtyoneticiemail = new System.Windows.Forms.TextBox();
            this.txtyoneticisifre = new System.Windows.Forms.TextBox();
            this.btnyoneticigiris = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.Location = new System.Drawing.Point(13, 59);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(38, 13);
            this.lblemail.TabIndex = 0;
            this.lblemail.Text = "E-mail:";
            this.lblemail.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(86, 52);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(100, 20);
            this.txtemail.TabIndex = 1;
            this.txtemail.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblsifre
            // 
            this.lblsifre.AutoSize = true;
            this.lblsifre.Location = new System.Drawing.Point(13, 119);
            this.lblsifre.Name = "lblsifre";
            this.lblsifre.Size = new System.Drawing.Size(31, 13);
            this.lblsifre.TabIndex = 2;
            this.lblsifre.Text = "Şifre:";
            // 
            // txtSifre
            // 
            this.txtSifre.Location = new System.Drawing.Point(86, 112);
            this.txtSifre.Name = "txtSifre";
            this.txtSifre.PasswordChar = '*';
            this.txtSifre.Size = new System.Drawing.Size(100, 20);
            this.txtSifre.TabIndex = 3;
            // 
            // giris
            // 
            this.giris.Location = new System.Drawing.Point(111, 196);
            this.giris.Name = "giris";
            this.giris.Size = new System.Drawing.Size(75, 23);
            this.giris.TabIndex = 4;
            this.giris.Text = "Giriş";
            this.giris.UseVisualStyleBackColor = true;
            this.giris.Click += new System.EventHandler(this.giris_Click);
            // 
            // lblkullanici
            // 
            this.lblkullanici.AutoSize = true;
            this.lblkullanici.Location = new System.Drawing.Point(86, 13);
            this.lblkullanici.Name = "lblkullanici";
            this.lblkullanici.Size = new System.Drawing.Size(71, 13);
            this.lblkullanici.TabIndex = 5;
            this.lblkullanici.Text = "Kullanıcı Girişi";
            // 
            // lblyonetici
            // 
            this.lblyonetici.AutoSize = true;
            this.lblyonetici.Location = new System.Drawing.Point(349, 13);
            this.lblyonetici.Name = "lblyonetici";
            this.lblyonetici.Size = new System.Drawing.Size(70, 13);
            this.lblyonetici.TabIndex = 6;
            this.lblyonetici.Text = "Yonetici Girişi";
            // 
            // lblyoneticiemail
            // 
            this.lblyoneticiemail.AutoSize = true;
            this.lblyoneticiemail.Location = new System.Drawing.Point(298, 58);
            this.lblyoneticiemail.Name = "lblyoneticiemail";
            this.lblyoneticiemail.Size = new System.Drawing.Size(38, 13);
            this.lblyoneticiemail.TabIndex = 7;
            this.lblyoneticiemail.Text = "E-mail:";
            // 
            // lblyoneticisifre
            // 
            this.lblyoneticisifre.AutoSize = true;
            this.lblyoneticisifre.Location = new System.Drawing.Point(298, 119);
            this.lblyoneticisifre.Name = "lblyoneticisifre";
            this.lblyoneticisifre.Size = new System.Drawing.Size(31, 13);
            this.lblyoneticisifre.TabIndex = 8;
            this.lblyoneticisifre.Text = "Şifre:";
            // 
            // txtyoneticiemail
            // 
            this.txtyoneticiemail.Location = new System.Drawing.Point(352, 51);
            this.txtyoneticiemail.Name = "txtyoneticiemail";
            this.txtyoneticiemail.Size = new System.Drawing.Size(100, 20);
            this.txtyoneticiemail.TabIndex = 9;
            // 
            // txtyoneticisifre
            // 
            this.txtyoneticisifre.Location = new System.Drawing.Point(352, 111);
            this.txtyoneticisifre.Name = "txtyoneticisifre";
            this.txtyoneticisifre.PasswordChar = '*';
            this.txtyoneticisifre.Size = new System.Drawing.Size(100, 20);
            this.txtyoneticisifre.TabIndex = 10;
            // 
            // btnyoneticigiris
            // 
            this.btnyoneticigiris.Location = new System.Drawing.Point(377, 196);
            this.btnyoneticigiris.Name = "btnyoneticigiris";
            this.btnyoneticigiris.Size = new System.Drawing.Size(75, 23);
            this.btnyoneticigiris.TabIndex = 11;
            this.btnyoneticigiris.Text = "Giriş";
            this.btnyoneticigiris.UseVisualStyleBackColor = true;
            this.btnyoneticigiris.Click += new System.EventHandler(this.btnyoneticigiris_Click);
            // 
            // KullaniciGiris
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 261);
            this.Controls.Add(this.btnyoneticigiris);
            this.Controls.Add(this.txtyoneticisifre);
            this.Controls.Add(this.txtyoneticiemail);
            this.Controls.Add(this.lblyoneticisifre);
            this.Controls.Add(this.lblyoneticiemail);
            this.Controls.Add(this.lblyonetici);
            this.Controls.Add(this.lblkullanici);
            this.Controls.Add(this.giris);
            this.Controls.Add(this.txtSifre);
            this.Controls.Add(this.lblsifre);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.lblemail);
            this.Name = "KullaniciGiris";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.KullaniciGiris_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label lblsifre;
        private System.Windows.Forms.TextBox txtSifre;
        private System.Windows.Forms.Button giris;
        private System.Windows.Forms.Label lblkullanici;
        private System.Windows.Forms.Label lblyonetici;
        private System.Windows.Forms.Label lblyoneticiemail;
        private System.Windows.Forms.Label lblyoneticisifre;
        private System.Windows.Forms.TextBox txtyoneticiemail;
        private System.Windows.Forms.TextBox txtyoneticisifre;
        private System.Windows.Forms.Button btnyoneticigiris;
    }
}

